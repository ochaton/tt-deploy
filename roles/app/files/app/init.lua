#!/usr/bin/env tarantool
require 'strict'.on()
local fiber = require 'fiber'
local fun = require 'fun'
local log = require 'log'
local me = debug.getinfo(1, "S").source:sub(2)
local fio = require 'fio'
local root = fio.dirname(assert(fio.readlink(fio.abspath(me))))
package.setsearchroot(root)

require 'package.reload'
require 'config' {
	master_selection_policy = 'etcd.cluster.master',
	file          = '/etc/app/conf.lua',
	mkdir         = true,
	instance_name = fio.basename(me):gsub("%.lua", ""),
	on_load       = function(_,cfg)
		if cfg.box.election_mode then
			cfg.box.read_only = false
			cfg.box.replication_synchro_quorum = "N/2+1"
			cfg.box.replication_connect_quorum = 2
		end
	end;
}

require 'metrics'.enable_default_metrics()

if not box.info.ro then
	box.schema.user.grant('guest', 'super', nil, nil, { if_not_exists = true })

	box.schema.space.create('T', {if_not_exists = true})
	box.space.T:create_index('I', { if_not_exists = true })

	if not box.space.T.is_sync then
		box.space.T:alter{is_sync=true}
	end

	box.space.T:format({
		{ name = 'id', type = 'unsigned' },
		{ name = 'rid', type = 'number' },
		{ name = 'time', type = 'number' },
		{ name = 'vclock', type = 'any' },
		{ name = 'election', type = 'any' },
	})

	fiber.create(function(gen)
		fiber.name('ttl/'..gen)

		while package.reload.count == gen do
			box.begin()
				local n_collected = box.space.T:pairs():enumerate()
					:map(function(no, t) if no % 1100 == 0 then box.commit() fiber.sleep(0) box.begin() end return t end)
					:take_while(function(t) return t.time < os.time()-60 end)
					:map(function(t) return box.space.T:delete{t.id} end)
					:length()
			box.commit()

			log.info("Deleted %d records", n_collected)
			fiber.sleep(60)
		end

	end, package.reload.count)
end

fiber.create(function()
	fiber.name('loader')
	local instances = fun.iter(config.etcd:get_instances())
		:map(function(_, i) return i.box.listen end)
		:totable()

	log.info("instances: %s", table.concat(instances))

	for _, inst in ipairs(instances) do
		fiber.create(function(gen)
			fiber.name('load/'..inst)
			local tt = require 'net.box'.connect(inst, { reconnect_after = 0.1 })

			while package.reload.count == gen do
				local ok, err = pcall(function()
					if not tt:call('box.info').ro then
						tt:eval[[box.space.T:insert{(box.space.T.index.I:max() or {0})[1] + 1, box.info.id, os.time(), box.info.vclock, box.info.election}]]
					end
				end)

				if not ok then
					log.error("eval: %s", err)
				end

				-- fiber.sleep(config.get('app.sleep', 0.1))
			end
		end, package.reload.count)
	end
end)
