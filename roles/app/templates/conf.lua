assert(instance_name, "instance_name is required")
etcd = {
	prefix = "/apps/app",
	endpoints = {
		"http://tt-1:2379",
		"http://tt-2:2379",
		"http://tt-3:2379",
	},
	instance_name = instance_name,
	boolean_auto = true,
	timeout = 3,
	dc = "{{ansible_hostname}}",
}

box = {
    background = true,
    log = 'syslog:server=unix:/dev/log,identity=app',
    pid_file = "/var/run/tarantool/" .. instance_name .. ".pid",
    memtx_dir = "/var/lib/tarantool/snaps/" .. instance_name,
    wal_dir = "/var/lib/tarantool/xlogs/" .. instance_name,
    log_nonblock = false
}